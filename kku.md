# 핫 픽스

git checkout -b hotfix/kku && git add . && git commit -m "fix: 포트 변경 " && git checkout develop && git merge --no-ff hotfix/kku && git push origin develop && git branch -d hotfix/kku && npm run start:dev

# 기능

git checkout -b feature/kku && git add . && git commit -m "feat: 게시물 저장 엔터티, Model And View 세팅" && git checkout develop && git merge --no-ff feature/kku && git push origin develop && git branch -d feature/kku && npm run start:dev

# Slack 연결
