import { CacheModule, MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { LoggerMiddleware } from './middleware/logger.middleware'
import { LoggerFriendMiddleware } from './middleware/logger-friends.middleware'
import { ConfigModule } from '@nestjs/config'
import { MulterModule } from '@nestjs/platform-express'
import { AuthModule } from './modules/auth/auth.module'
import { TypeOrmModule } from '@nestjs/typeorm'
import * as Joi from 'joi'
import { LoggerEntity } from './modules/auth/entities/logger.entity'
import { JwtService } from '@nestjs/jwt'
import multerRegister from './common/multer/register.multer'
import { ExceptionModule } from './common/exceptions/logger-exception.filter'
import { KKuIslandDataSource } from './config/orm.config'
import { ServeStaticModule } from '@nestjs/serve-static'
import { join } from 'path'
import { EventEmitterModule } from '@nestjs/event-emitter'
import { OrdersModule } from './modules/orders/orders.module'
import { NotionModule } from './modules/notion/notion.module'
import { PostModule } from './modules/post/post.module'
import { PhotoModule } from './modules/photo/photo.module'
import { GraphQLModule } from '@nestjs/graphql'
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo'
import { FriendModule } from './modules/auth/Friend.module'

@Module({
	imports: [
		// 알림: Config 설정
		ConfigModule.forRoot({
			isGlobal: true,
			envFilePath: ['.env'],
			validationSchema: Joi.object({
				NODE_ENV: Joi.string().valid('development', 'product', 'test', 'provision').default('development'),
				NODE_PORT: Joi.number().default(9000),
				TZ: Joi.string().default('Asia/Seoul'),
				BASE_URL: Joi.string().default('https://localhost'),
				NODE_MAILER_ID: Joi.string().default(''),
				NODE_MAILER_PW: Joi.string().default(''),
				ATTACH_SAVE_PATH: Joi.string().default('/public'),
				DB_KKUISLAND_HOST: Joi.string().required(),
				DB_KKUISLAND_PORT: Joi.number().required(),
				DB_KKUISLAND_USERNAME: Joi.string().required(),
				DB_KKUISLAND_PASSWORD: Joi.string().required(),
				DB_KKUISLAND_DATABASE: Joi.string().required(),
				MONGODB_URL: Joi.string().default(''),
				JWT_SECRET: Joi.string().required(),
				JWT_EXPIRATION_TIME: Joi.number().default(24),
				COOKIE_NAME: Joi.string().default('KKuToken'),
				COOKIE_SECRET: Joi.string().required(),
				COOKIE_EXPIRATION_TIME: Joi.number().default(24),
			}),
		}),
		ServeStaticModule.forRoot({
			rootPath: join(__dirname, '..', 'client'),
		}),
		// 알림: 캐쉬설정
		CacheModule.register({
			isGlobal: true,
			ttl: 5000,
		}),
		EventEmitterModule.forRoot({
			// 와일드 카드를 사용하려면 이것을 'true'로 설정하십시오.
			wildcard: false,
			// 네임스페이스를 분할하는 데 사용되는 구분 기호
			delimiter: '.',
			// newListener 이벤트를 내보내려면 이것을 'true'로 설정하십시오.
			newListener: false,
			//removeListener 이벤트를 내보내려면 이것을 'true'로 설정하십시오.
			removeListener: false,
			// 이벤트에 할당할 수 있는 최대 리스너 수
			maxListeners: 10,
			// 최대 리스너가 할당된 경우 메모리 누수 메시지에 이벤트 이름 표시
			verboseMemoryLeak: false,
			// 오류 이벤트가 발생하고 리스너가 없는 경우 uncaughtException 발생 비활성화
			ignoreErrors: false,
		}),
		// 알림: 파일 관리 설정
		MulterModule.register(multerRegister),
		// 알림: TypeOrm
		TypeOrmModule.forRoot(KKuIslandDataSource),
		TypeOrmModule.forFeature([LoggerEntity]),
		GraphQLModule.forRoot<ApolloDriverConfig>({
			driver: ApolloDriver,
			debug: true,
			playground: true,
			// autoSchemaFile: true,
			// sortSchema: true,
			typePaths: ['src/modules/**/graphql/*.graphql'],
			// definitions: {
			// 	path: join(process.cwd(), 'src/graphql.ts'),
			// 	outputAs: 'class',
			// },
		}),
		// 모듈's
		AuthModule,
		// CafeModule,
		ExceptionModule,
		OrdersModule,
		NotionModule,
		PostModule,
		PhotoModule,
		FriendModule,
	],
	controllers: [],
	providers: [JwtService],
})
export class AppModule implements NestModule {
	configure(consumer: MiddlewareConsumer): void {
		consumer.apply(LoggerMiddleware).forRoutes('*')
		consumer.apply(LoggerFriendMiddleware).forRoutes('/friends')
	}
}
