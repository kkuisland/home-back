import { Module } from '@nestjs/common'
import { JwtModule, JwtService } from '@nestjs/jwt'
import { PassportModule } from '@nestjs/passport'
import { TypeOrmModule } from '@nestjs/typeorm'
import { JwtStrategy } from 'src/common/jwt/jwt.strategy'
import { EmailService } from 'src/common/nodemailer/nodemailer.service'
import { KKuIslandJwtOptions } from 'src/config/jwt.config'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { FriendEntity } from './entities/friend.entity'
import { PasswordEntity } from './entities/passwords.entity'
import { RoleEntity } from './entities/role.entity'
import { TestEntity } from './entities/test.entity'
import { FriendController } from './friend.controller'
import { FriendService } from './friend.service'

@Module({
	imports: [
		TypeOrmModule.forFeature([FriendEntity, PasswordEntity, RoleEntity, TestEntity]),
		JwtModule.register(KKuIslandJwtOptions),
		PassportModule,
	],
	controllers: [FriendController, AuthController],
	providers: [AuthService, FriendService, EmailService, JwtService, JwtStrategy],
})
export class AuthModule {}
