import {
	Body,
	Controller,
	Delete,
	Get,
	HttpStatus,
	Param,
	ParseArrayPipe,
	Post,
	Put,
	Query,
	Res,
	UseGuards,
} from '@nestjs/common'
import { ApiBody, ApiCreatedResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { CommonNotice, FriendRole } from 'src/common/enums'
import { Public } from 'src/common/decorators/public.decorator'
import { Roles } from 'src/common/decorators/role.decorator'
import { RolesGuard } from 'src/common/guard/roles.guard'
import { CreateFriendDto } from './dto/create-friend.dto'
import { UpdateFriendDto } from './dto/update-friend.dto'
import { VerifyEmailDto } from './dto/verify-email.dto'
import { FriendService } from './friend.service'
import ResponseSend from 'src/common/response/default.response'
import { BadFriend } from './enums/bad-friends.enum'

@Controller('friend')
@ApiTags('친구 API')
export class FriendController {
	constructor(private readonly friendService: FriendService) {}

	@Post()
	@Public()
	@ApiOperation({
		summary: '친구 생성',
		description: '친구 생성을 진행합니다.',
	})
	@ApiCreatedResponse({
		description: '친구 생성',
		schema: {
			example: {
				identity: 'kku',
				name: '구인수',
				email: 'kkuinsoo@gmail.com',
				mobile: '010-9947-0728',
				password: '11qqaa..',
			},
		},
	})
	async create(@Res() res: Response, @Body() createFriendDto: CreateFriendDto): Promise<Response> {
		const friend = await this.friendService.create(createFriendDto)
		return res.send(ResponseSend.default(HttpStatus.CREATED, CommonNotice.CREATED_SEND, { friend }))
	}

	@Post('/array')
	@Public()
	@ApiOperation({
		summary: '친구들 생성',
		description: '친구들 생성을 진행합니다.',
	})
	@ApiCreatedResponse({
		description: '친구들 생성',
		schema: {
			example: [
				{
					identity: 'kku',
					name: '구인수',
					email: 'kkuinsoo@gmail.com',
					mobile: '010-9947-0728',
					password: '11qqaa..',
				},
			],
		},
	})
	@ApiBody({ type: [CreateFriendDto] })
	async createa(
		@Res() res: Response,
		@Body(new ParseArrayPipe({ items: CreateFriendDto })) createFriendDto: CreateFriendDto[]
	): Promise<Response> {
		const friend = await this.friendService.createa(createFriendDto)
		return res.send(ResponseSend.default(HttpStatus.CREATED, CommonNotice.CREATED_SEND, { friend }))
	}

	@Get()
	@Public()
	@ApiOperation({
		summary: "친구's 찾기",
		description: '등록된 친구들을 찾습니다.',
	})
	@ApiCreatedResponse({
		description: "친구's 찾기",
		schema: {
			example: {},
		},
	})
	@UseGuards(RolesGuard)
	@Roles(FriendRole.ADMIN)
	async findAll(@Res() res: Response): Promise<Response> {
		const friends = await this.friendService.findAll()
		return res.send(ResponseSend.default(HttpStatus.OK, CommonNotice.FINDALL_SEND, { friends }))
	}

	@Get('/:identity')
	@Public()
	@ApiOperation({
		summary: '친구 아이디 찾기',
		description: '친구 아이디로 등록된 친구를 찾습니다.',
	})
	@ApiCreatedResponse({
		description: '친구 찾기',
		schema: {
			example: {},
		},
	})
	@ApiParam({ name: 'identity', enum: BadFriend })
	async findOne(@Res() res: Response, @Param('identity') identity: BadFriend = BadFriend.꾸인수): Promise<Response> {
		const friend = await this.friendService.findOne(identity)
		return res.send(ResponseSend.default(HttpStatus.OK, CommonNotice.FINDONE_SEND, friend))
	}

	@Put()
	@ApiOperation({
		summary: '친구 수정',
		description: '친구 정보를 수정합니다.',
	})
	@ApiCreatedResponse({
		description: '친구 수정',
		schema: {
			example: {},
		},
	})
	async update(@Res() res: Response, @Body() updateFriendDto: UpdateFriendDto): Promise<Response> {
		const friend = await this.friendService.update(updateFriendDto)
		return res.send(ResponseSend.default(HttpStatus.OK, CommonNotice.UPDATED_SEND, friend))
	}

	@Delete(':id')
	@ApiOperation({
		summary: '친구 삭제',
		description: '친구 정보를 제거합니다. ( DB 삭제 진행은 되지않습니다. )',
	})
	@ApiCreatedResponse({
		description: '친구 삭제',
		schema: {
			example: {},
		},
	})
	async remove(@Res() res: Response, @Param('id') id: number): Promise<Response> {
		const result = await this.friendService.remove(id)

		return res.send(ResponseSend.default(HttpStatus.OK, CommonNotice.DELETE_SEND, result))
	}

	@Post('/email-verify')
	async verifyEmail(@Query() emailDto: VerifyEmailDto): Promise<string> {
		// const { signupVerifyToken } = emailDto;

		return await this.friendService.verifyEmail(emailDto)
	}

	@Get('/test/service')
	@Public()
	@ApiOperation({
		summary: '[ Public ] 테스트 서비스',
		description: '테스트 API',
	})
	async testService(@Res() res: Response) {
		const test = await this.friendService.TestService()
		return res.send(ResponseSend.default(HttpStatus.OK, '테스트 서비스', { test }))
	}
}
