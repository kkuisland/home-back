import { BaseEntity, Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { FriendRole } from '../../../common/enums/friend-role.enum'
import { FriendEntity } from './friend.entity'

@Entity('role')
export class RoleEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: '역할 인덱스 Auto Increment' })
	id!: number

	@Column({ type: 'enum', enum: FriendRole, default: null, nullable: true, comment: '권한' })
	role!: FriendRole

	@ManyToOne(() => FriendEntity, (friend) => friend.roles, {
		cascade: true,
		onUpdate: 'CASCADE',
		onDelete: 'CASCADE',
		persistence: false,
	})
	@JoinColumn({ name: 'friend' })
	friend!: number
}
