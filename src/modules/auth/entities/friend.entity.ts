import {
	BaseEntity,
	Column,
	Entity,
	JoinColumn,
	OneToMany,
	OneToOne,
	PrimaryGeneratedColumn,
	RelationId,
} from 'typeorm'
import { LoggerEntity } from './logger.entity'
import { PasswordEntity } from './passwords.entity'
import { RoleEntity } from './role.entity'

@Entity('friend')
export class FriendEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: '친구 인덱스 Auto Increment ' })
	id!: number

	@Column({ length: 20, unique: true, comment: '아이디' })
	identity!: string

	@Column({ length: 20, unique: true, comment: '이메일' })
	email!: string

	@Column({ length: 20, unique: true, comment: '모바일' })
	mobile!: string

	@Column({ length: 20, comment: '이름' })
	name!: string

	@OneToOne(() => PasswordEntity, (password) => password.friend, { persistence: false, eager: true })
	@JoinColumn()
	password!: PasswordEntity

	@OneToMany(() => RoleEntity, (role) => role.friend, { eager: true, persistence: false })
	roles!: RoleEntity[]

	@OneToMany(() => LoggerEntity, (logger) => logger.friend, { nullable: true, persistence: false })
	logger!: LoggerEntity[]
}
