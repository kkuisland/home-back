import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity({ name: 'test_service' })
export class TestEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: 'Auto Increment' })
	id!: number

	@Column({ type: 'varchar', length: 20, comment: '이름' })
	name!: string
}
