import { Column, CreateDateColumn, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, BaseEntity } from 'typeorm'
import { FriendEntity } from './friend.entity'

@Entity('logger')
export class LoggerEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: '로거 인덱스 Auto Increment' })
	id!: number

	@Column({ comment: '기본 경로' })
	baseUrl!: string

	@Column({ comment: '오리지널 경로' })
	originalUrl!: string

	@CreateDateColumn({ comment: '생성 일자' })
	createdAt!: Date

	@ManyToOne(() => FriendEntity, (friend) => friend.identity, {
		cascade: true,
		onUpdate: 'CASCADE',
		onDelete: 'SET NULL',
		persistence: false,
	})
	@JoinColumn({ name: 'friend' })
	friend!: number
}
