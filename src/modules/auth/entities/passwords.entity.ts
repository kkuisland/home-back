import { BaseEntity, BeforeInsert, BeforeUpdate, Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { FriendEntity } from './friend.entity'

import * as bcrypt from 'bcrypt'

const SALT_ROUND = 11
@Entity('password')
export class PasswordEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: '패스워드 인덱스 Auto Increment' })
	id!: number

	@Column({ comment: '해시 패스워드' })
	hash!: string

	@OneToOne(() => FriendEntity, (friend) => friend.password, {
		cascade: true,
		onUpdate: 'CASCADE',
		onDelete: 'CASCADE',
		persistence: false,
	})
	friend!: FriendEntity

	private async hashMaker(password: string): Promise<string> {
		return await bcrypt.hash(password, SALT_ROUND)
	}

	@BeforeInsert()
	@BeforeUpdate()
	async saveHash(): Promise<void> {
		if (this.hash) {
			const hashedPassword = await this.hashMaker(this.hash)
			this.hash = hashedPassword
		}
	}
}
