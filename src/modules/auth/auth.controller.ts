import { Body, Controller, Get, HttpStatus, Post, Req, Res } from '@nestjs/common'
import { Request, Response } from 'express'
import { Public } from 'src/common/decorators/public.decorator'
import { AuthService } from './auth.service'
import { LoginDto } from './dto/login.dto'
import ResponseSend from '../../common/response/default.response'
import { ApiDiscription } from '../../common/discription/dto.discription'
import { ApiOperation, ApiTags } from '@nestjs/swagger'

@Controller('auth')
@ApiTags('인증 API')
export class AuthController {
	constructor(private readonly authService: AuthService) {}

	/* 로그인 */
	@Post('/login')
	@Public()
	@ApiOperation({
		summary: '[ Public ] 로그인',
		description: ApiDiscription(
			'로그인 기능',
			'identity, password 를 입력받습니다.',
			'JWT 토큰에 friend 정보를 담아 HttpOnly Cookie 에 넣어 응답합니다.'
		),
	})
	async login(@Res() res: Response, @Body() loginDto: LoginDto): Promise<Response> {
		const payload = await this.authService.login(loginDto)

		res.cookie(process.env.COOKIE_NAME, payload.accessToken, {
			httpOnly: true,
			maxAge: (parseInt(process.env.COOKIE_EXPIRATION_TIME) ?? 3) * 60 * 60 * 1000, // 1day
			sameSite: 'none',
			path: '/',
			secure: true,
			signed: true,
		})

		const user = payload.user
		return res.send(ResponseSend.default(HttpStatus.CREATED, '로그인', user))
	}

	@Get()
	@ApiOperation({
		summary: '쿠키 체크',
		description: ApiDiscription('쿠키 JWT 확인'),
	})
	async check(@Res() res: Response, @Req() req: Request): Promise<Response> {
		const friend = await this.authService.check(req.signedCookies[process.env.COOKIE_NAME])
		return res.send(ResponseSend.default(HttpStatus.OK, '쿠키가 확인 되었습니다', { friend }))
	}

	/* 로그아웃 */
	@Get('/logout')
	@Public()
	@ApiOperation({ summary: '[ Public ] 로그아웃', description: ApiDiscription('로그아웃', 'Cookie 소멸') })
	logout(@Res() res: Response): Response {
		res.clearCookie(process.env.COOKIE_NAME, {
			httpOnly: true,
			maxAge: 0,
			sameSite: 'none',
			secure: true,
			path: '/',
		})
		return res.send(ResponseSend.default(HttpStatus.OK, '로그아웃'))
	}
}
