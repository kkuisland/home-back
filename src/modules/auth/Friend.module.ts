import { Module } from '@nestjs/common'
import { FriendResolver } from './model/friend.resolver'

@Module({
	imports: [],
	controllers: [],
	providers: [FriendResolver],
})
export class FriendModule {}
