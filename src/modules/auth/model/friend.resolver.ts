import { Query, Resolver } from '@nestjs/graphql'

@Resolver('Friend')
export class FriendResolver {
	@Query()
	async getFriends() {
		return [
			{
				id: '1',
				name: '구인수',
				identity: 'kku',
				mobile: '010-9947-0728',
				email: 'kkuinsoo@gmail.com',
			},
			// { id: '2', name: '변정원', mobile: '+82 10-8765-4321' },
		]
	}
}
