import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { LoginDto } from './dto/login.dto'
import { FriendService } from './friend.service'
import { Payload } from './types/payload.type'
import { CommonNotice, FriendNotice } from 'src/common/enums'
import { FriendEntity } from './entities/friend.entity'

@Injectable()
export class AuthService {
	constructor(private readonly friendService: FriendService, private readonly jwtService: JwtService) {}

	/**
	 * 로그인
	 * @param loginDto 로그인 DTO
	 * @returns 로그인 완료
	 */
	async login(loginDto: LoginDto): Promise<{ user: Payload; accessToken: string }> {
		// 아아디 조회
		const foundFriend = await FriendEntity.findOne({
			where: { identity: loginDto.identity },
			relations: { password: true },
		})
		if (!foundFriend) {
			throw new NotFoundException(FriendNotice.NOT_FOUND)
		}
		// 아이디와 패스워드 조회 후 패스워드 비교
		const resultComparePassword = await this.friendService.compareHashPassword(
			loginDto.password,
			foundFriend.password.id
		)
		if (!resultComparePassword) {
			throw new UnauthorizedException(CommonNotice.UNAUTHORIZED)
		}
		// const foundFriendRoles = await this.friendService.findroles(foundFriend.id)
		// JWT 와 Cookie 생성
		const payload: Payload = {
			identity: foundFriend.identity,
			name: foundFriend.name,
			email: foundFriend.email,
			mobile: foundFriend.mobile,
			// roles: foundFriendRoles,
		}

		return {
			user: payload,
			accessToken: this.jwtService.sign(payload, {
				secret: process.env.JWT_SECRET,
				expiresIn: `${process.env.JWT_EXPIRATION_TIME ?? 3}d`,
			}),
		}
	}

	/**
	 * JWT 조회 하여 Payload 정보 제공
	 * @param jwt JWT
	 * @returns Payload
	 */
	async check(jwt: string): Promise<Payload> {
		const payload = this.jwtService.decode(jwt) as Payload
		const foundFriend = await this.friendService.findOne(payload.identity)

		return {
			identity: foundFriend.identity,
			name: foundFriend.name,
			email: foundFriend.email,
			mobile: foundFriend.mobile,
			// roles: foundFriendRoles,
		}
	}

	// --------------------------------- Validator ---------------------------------

	/**
	 * 토큰 인증 확인
	 * @param payload PalyLoad
	 * @returns 새로운 Palyload
	 */
	async tokenValidateFriend(payload: Payload): Promise<Payload | null> {
		const foundFriend = await this.friendService.findOne(payload.identity)
		if (!foundFriend) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		// const foundFriendroles = await this.friendService.findroles(payload.id)
		let jwtFriend: Payload = payload
		// if (foundFriend && foundFriend.roles) { 룰 없음
		if (foundFriend) {
			jwtFriend = {
				identity: foundFriend.identity,
				email: foundFriend.email,
				mobile: foundFriend.mobile,
				name: foundFriend.name,
			}
			return jwtFriend
		}
		return payload
	}
	// ---------------------------------- Private ----------------------------------
}
