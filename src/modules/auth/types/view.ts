import { BaseEntity, ViewColumn, ViewEntity } from 'typeorm'

@ViewEntity({
	expression: `select *, SUM(1+1) as sun from friend order by id asc`,
})
export class ViewFriend extends BaseEntity {
	@ViewColumn()
	id!: number

	@ViewColumn()
	identity!: string

	@ViewColumn()
	email!: string

	@ViewColumn()
	mobile!: string

	@ViewColumn()
	name!: string

	@ViewColumn()
	nameTwo!: string

	@ViewColumn()
	test!: string
}
