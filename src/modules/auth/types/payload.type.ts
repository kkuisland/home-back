export type Payload = {
	identity: string
	name: string
	email: string
	mobile: string
	// roles: RoleEntity[]
}
