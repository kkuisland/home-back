import { BadRequestException, CACHE_MANAGER, Inject, Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { DeleteResult, Repository } from 'typeorm'
import { CreateFriendDto } from './dto/create-friend.dto'
import { UpdateFriendDto } from './dto/update-friend.dto'
import * as bcrypt from 'bcrypt'
import { VerifyEmailDto } from './dto/verify-email.dto'
import { FriendEntity } from './entities/friend.entity'
import { PasswordEntity } from './entities/passwords.entity'
import { RoleEntity } from './entities/role.entity'
import { CommonNotice, FriendNotice, FriendRole } from 'src/common/enums'
import { EmailService } from 'src/common/nodemailer/nodemailer.service'
import { QueryException } from 'src/common/exceptions/query-exception.filter'
import { Cache } from 'cache-manager'
import { ViewFriend } from './types/view'
import { TestEntity } from './entities/test.entity'

@Injectable()
export class FriendService {
	constructor(
		@InjectRepository(FriendEntity) private readonly friendRepository: Repository<FriendEntity>,
		@InjectRepository(PasswordEntity) private readonly friendPasswordRepository: Repository<PasswordEntity>,
		@InjectRepository(RoleEntity) private readonly friendRolesRepository: Repository<RoleEntity>,
		private readonly emailService: EmailService,
		@Inject(CACHE_MANAGER) private cacheManager: Cache
	) {}

	/**
	 * 친구를 생성합니다.
	 * @param createFriendDto 친구 생성 DTO
	 * @returns 생성 된 친구
	 */
	async create(createFriendDto: CreateFriendDto): Promise<FriendEntity> {
		// 아이디 중복
		await this.checkDuplicateValues(createFriendDto.identity, createFriendDto.mobile, createFriendDto.email)
		const createFriend = await this.saveFriendTransaction(createFriendDto)
		return createFriend
	}

	/**
	 * 친구를 생성합니다.
	 * @param createFriendDto 친구 생성 DTO
	 * @returns 생성 된 친구
	 */
	async createa(createFriendDto: CreateFriendDto[]): Promise<FriendEntity> {
		// 아이디 중복
		await this.checkDuplicateValues(createFriendDto[0].identity, createFriendDto[0].mobile, createFriendDto[0].email)
		const createFriend = await this.saveFriendTransaction(createFriendDto[0])
		return createFriend
	}

	/**
	 * 친구's 검색 ( farmId )
	 * @returns 친구's 정보
	 */
	async findAll(): Promise<FriendEntity[]> {
		// 캐쉬 확인
		// const cacheFriend = (await this.cacheManager.get('friends')) as FriendEntity[]
		// if (cacheFriend) {
		// 	console.debug('cash 📝')
		// 	return cacheFriend
		// }
		// const query = (await this.friendRepository.query(
		// 	`select *, SUM(1+1) as sun from friend order by id asc`
		// )) as FriendEntity[]
		// console.debug('q', query)

		const query = (await this.friendRepository.query(
			`select *, SUM(1+1) as test from friend order by id asc`
		)) as ViewFriend[]
		console.debug('q', query)
		console.debug(query[0].test)
		const foundFriend = await this.friendRepository.find({ order: { id: 'ASC' } })
		if (!foundFriend) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		// console.debug('set 📝')
		// await this.cacheManager.set('friends', foundFriend)
		return foundFriend
	}

	/**
	 * 하나의 친구 정보 검색
	 * @param  identity 친구 아이디
	 * @returns 친구 정보
	 */
	async findOne(identity: string): Promise<FriendEntity> {
		const foundFriendOne = await this.friendRepository.findOne({
			where: { identity: identity },
		})
		if (!foundFriendOne) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		// await this.cacheManager.set('key', foundFriendOne)
		// const cache = await this.cacheManager.get('key')
		// console.log('cache get', cache)
		return foundFriendOne
	}

	/**
	 * 친구 인덱스로 패스워드 검색
	 * @param  friendId 친구 아이디
	 * @returns 친구 정보
	 */
	async findFriendWidthPassword(friendId: number): Promise<PasswordEntity> {
		const foundFriendOne = await this.friendPasswordRepository.findOne({
			where: { id: friendId },
			relations: { friend: true },
		})
		if (!foundFriendOne) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		return foundFriendOne
	}

	/**
	 * 친구 정보 업데이트 ( id )
	 * @param updateFriendDto 업데이트 친구 DTO
	 * @returns 업데이트 된 친구 객체
	 */
	async update(updateFriendDto: UpdateFriendDto): Promise<FriendEntity> {
		const friend = await this.friendRepository.findOne({ where: { identity: updateFriendDto.identity } })
		if (!friend) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}

		const updatedFriend = await this.updateFriendTransaction(updateFriendDto)
		return updatedFriend
	}

	/**
	 * 친구 제거
	 * @param id 친구 인덱스
	 * @returns 삭제 결과
	 */
	async remove(id: number): Promise<DeleteResult> {
		const deleteResult = await this.friendRepository.delete(id)
		return deleteResult
	}

	/**
	 * 이메일 전송
	 * @param emailDto Email Dto
	 * @returns 인증 번호
	 */
	async verifyEmail(emailDto: VerifyEmailDto): Promise<string> {
		await this.sendMemberJoinEmail(emailDto.emailAddress, emailDto.signupVerifyToken)
		return emailDto.emailAddress
	}

	/* 친구 권한 */
	async findroles(friendId: number): Promise<RoleEntity[]> {
		const foundFriendRoles = await this.friendRolesRepository.find({
			where: { id: friendId },
			order: { role: 'ASC' },
		})

		if (foundFriendRoles.length === 0) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		return foundFriendRoles
	}

	/* 테스트 서비스 */
	async TestService(): Promise<TestEntity[]> {
		const test = new TestEntity()
		test.name = '구인수'
		await test.save()

		const result = await TestEntity.find()
		result.forEach(async (item) => await item.remove())

		return []
	}
	// --------------------------------- Validator ---------------------------------
	private async checkDuplicateValues(identity?: string, mobile?: string, email?: string) {
		console.log(identity)
		if (identity) {
			const foundFriend = await this.friendRepository.findOne({ where: { identity: identity } })
			if (foundFriend) throw new NotFoundException(FriendNotice.CONFLICT_ID)
		}
		if (mobile) {
			const foundFriend = await this.friendRepository.findOne({ where: { mobile: mobile } })
			if (foundFriend) throw new NotFoundException(FriendNotice.CONFLICT_MOBILE)
		}
		if (email) {
			const foundFriend = await this.friendRepository.findOne({ where: { email: email } })
			if (foundFriend) throw new NotFoundException(FriendNotice.CONFLICT_EMAIL)
		}
	}

	private async sendMemberJoinEmail(email: string, signupVerifyToken: string) {
		await this.emailService.sendMemberJoinVerification(email, signupVerifyToken)
	}

	/**
	 * 패스워드를 해시패스워드로 만든다.
	 * @param password 패스워드
	 * @param saltRound 섞는 횟수
	 * @returns 해시 패스워드
	 */
	private async makeHashPassword(password: string, saltRound: number): Promise<string> {
		const hashpassword = await bcrypt.hash(password, saltRound)
		return hashpassword
	}

	/**
	 * 패스워드와 기존 해시 패스워드를 비교하여 결과를 확인한다.
	 * @param password 비교 패스워드
	 * @param friendIndex 해시 패스워드 검색 인덱스
	 * @returns 결과
	 */
	async compareHashPassword(password: string, passwordId: number): Promise<boolean> {
		const hashpassword = await PasswordEntity.findOne({ where: { id: passwordId } })
		if (!hashpassword) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		const compare = await bcrypt.compare(password, hashpassword.hash)
		return compare
	}
	// ---------------------------------- Private ----------------------------------
	/**
	 * 친구 생성 ( Transaction )
	 * @param friendInfo 친구 정보
	 */
	private async saveFriendTransaction(friendInfo: CreateFriendDto): Promise<FriendEntity> {
		const mobileNumber = friendInfo.mobile.replace(/[^0-9]/g, '')
		const hyphenMobileNumber = mobileNumber.replace(/^(\d{2,3})(\d{3,4})(\d{4})$/, `$1-$2-$3`)
		// 친구 트랜젝션
		return await this.friendRepository.manager.transaction(async (transcationalEntityManager) => {
			// 친구 생성
			try {
				const createFriend = this.friendRepository.create({
					identity: friendInfo.identity,
					email: friendInfo.email,
					mobile: hyphenMobileNumber,
					name: friendInfo.name,
				})
				const createdFriend = await transcationalEntityManager.save(createFriend)

				// 생성된 친구 아이디로 친구-패스워드 생성
				const createFriendPasswod = this.friendPasswordRepository.create({
					// friend: createdFriend.id,
					hash: friendInfo.password,
					// hash: await this.makeHashPassword(friendInfo.password, 11),
				})

				await transcationalEntityManager.save(createFriendPasswod)

				const createFriendAuthority = this.friendRolesRepository.create({
					friend: createFriend.id,
					role: FriendRole.FRIEND,
				})

				await transcationalEntityManager.save(createFriendAuthority)

				return createdFriend
			} catch (error) {
				throw QueryException.create(error)
			}
		})
	}
	/**
	 * 친구 업데이트 ( Transaction )
	 * @param friendInfo 친구 정보
	 * @returns 업데이트 된 친구 정보
	 */
	private async updateFriendTransaction(friendInfo: UpdateFriendDto): Promise<FriendEntity> {
		// 유저 트랜젝션
		const updatedResult = await this.friendRepository.manager.transaction(async (transcationalEntityManager) => {
			const updateFriend = await transcationalEntityManager.update(
				FriendEntity,
				{ identity: friendInfo.identity },
				{
					email: friendInfo.email,
					mobile: friendInfo.mobile,
					name: friendInfo.name,
				}
			)
			return updateFriend
		})

		if (updatedResult.affected === 0) {
			throw new BadRequestException(CommonNotice.UPDATE_FAILED)
		}
		const updatedFriend = await this.friendRepository.findOne({ where: { identity: friendInfo.identity } })
		if (!updatedFriend) {
			throw new NotFoundException(CommonNotice.NOT_FOUND)
		}
		return updatedFriend
	}
}
