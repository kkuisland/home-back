import { ApiProperty } from '@nestjs/swagger'
import { Matches } from 'class-validator'
import { FriendNotice } from 'src/common/enums'
export class LoginDto {
	@Matches(/^[a-z]+[a-z0-9]{2,19}$/, { message: FriendNotice.IDENTITY })
	@ApiProperty({ description: '친구 아이디 | 정규식: /^[a-z]+[a-z0-9]{2,19}$/g', example: 'kku' })
	identity!: string

	@Matches(/^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z.$`~!@$!%*#^?&\\(\\)\-_=+]{6,20}$/, {
		message: FriendNotice.PASSWORD,
	})
	@ApiProperty({
		description: '친구 비밀번호 | 정규식: /^(?=.*d)(?=.*[a-zA-Z])[0-9a-zA-Z&#96;.$~!@$!%*#^?&\\(\\)-_=+]{6,20}$/',
		example: '11qqaa..',
	})
	password!: string
}
