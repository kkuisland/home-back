import { ApiProperty } from '@nestjs/swagger'
import { Matches } from 'class-validator'
import { DtoDiscription } from 'src/common/discription'
import { FriendNotice } from 'src/common/enums'
export class CreateFriendDto {
	@Matches(/^[a-z]+[a-z0-9]{2,19}$/, { message: FriendNotice.IDENTITY })
	@ApiProperty({ description: DtoDiscription('친구 아이디', '/^[a-z]+[a-z0-9]{2,19}$/g'), example: 'kku' })
	identity!: string

	@Matches(/^[a-zA-Zㄱ-힣]{2,19}$/, { message: FriendNotice.NAME })
	@ApiProperty({
		description: DtoDiscription('친구 이름', '/^[a-zA-Zㄱ-힣]{2,19}$/'),
		example: '구인수',
	})
	name!: string

	// @IsEmail()
	@Matches(/^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z]{3,20})*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i, {
		message: FriendNotice.EMAIL,
	})
	@ApiProperty({
		description: DtoDiscription(
			'친구 이메일',
			'/^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z]{3,20})*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i'
		),
		example: 'kkuisland@gmail.com',
	})
	email!: string

	@Matches(/^01(?:0|1|[6-9])-?(?:\d{3}|\d{4})-?\d{4}$/, {
		message: FriendNotice.MOBILE,
	})
	@ApiProperty({
		description: DtoDiscription('친구 모바일 번호', '/^01([0|1|6|7|8|9])-?([0-9]{3,4})-?([0-9]{4})$/'),
		example: '01099470728',
	})
	mobile!: string

	@Matches(/^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z.$`~!@$!%*#^?&\\(\\)\-_=+]{6,20}$/, {
		message: FriendNotice.PASSWORD,
	})
	@ApiProperty({
		description: DtoDiscription(
			'친구 비밀번호',
			'/^(?=.*d)(?=.*[a-zA-Z])[0-9a-zA-Z&#96;.$~!@$!%*#^?&\\(\\)-_=+]{6,20}$/'
		),
		example: '11qqaa..',
	})
	password!: string
}
