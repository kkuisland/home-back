import { ApiProperty, PartialType } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber } from 'class-validator'
import { DtoDiscription } from 'src/common/discription'
import { CreateFriendDto } from './create-friend.dto'

export class UpdateFriendDto extends PartialType(CreateFriendDto) {
	@IsNumber()
	@IsNotEmpty()
	@ApiProperty({
		description: DtoDiscription('친구 인덱스', 'Auto Increment'),
		example: '1',
	})
	id!: number
}
