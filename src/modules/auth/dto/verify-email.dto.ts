import { ApiProperty } from '@nestjs/swagger'
import { IsEmail, IsString } from 'class-validator'

export class VerifyEmailDto {
	@IsEmail()
	@ApiProperty({ description: '이메일', example: 'kkuisland@gmail.com' })
	emailAddress!: string

	@IsString()
	@ApiProperty({ description: '임시토큰', example: 'abcdefg12345' })
	signupVerifyToken!: string
}
