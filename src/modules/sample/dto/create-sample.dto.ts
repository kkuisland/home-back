import { ApiProperty } from '@nestjs/swagger'
import { Transform } from 'class-transformer'
import { IsString } from 'class-validator'

export class CreateSampleDto {
	@IsString()
	@Transform((params) => params.value.trim())
	@ApiProperty({ description: '샘플명', example: 'Sample Name' })
	title!: string

	@IsString()
	@Transform((params) => params.value.trim())
	@ApiProperty({ description: '샘플 내용', example: 'Sample Contents' })
	contents!: string
}
