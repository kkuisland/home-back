import { Controller, Get, Param, Post, Query, Res, UploadedFile, UseInterceptors } from '@nestjs/common'
import { FileInterceptor } from '@nestjs/platform-express'
import { ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger'
import { Response } from 'express'
import { AppService } from './app.service'

@Controller('user')
@ApiTags('샘플 API')
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get('/login')
	async getHello(@Res() res: Response): Promise<Response> {
		return res.send({
			statusCode: 200,
			message: this.appService.getHello(),
		})
	}

	@Post('/upload')
	@UseInterceptors(FileInterceptor('file'))
	// 👈 field name must match
	@ApiConsumes('multipart/form-data')
	@ApiBody({
		schema: {
			type: 'object',
			properties: {
				file: {
					// 👈 this property
					type: 'string',
					format: 'binary',
				},
			},
		},
	})
	uploadFile(@UploadedFile() file: Express.Multer.File): any {
		const path = file.path.replace(process.env.ATTACH_SAVE_PATH, '')

		// 원본파일명과 저장된 파일명을 리턴해서 다운로드 받을때 씀
		return {
			fileName: file.originalname,
			savedPath: path.replace(/\\/gi, '/'),
			size: file.size,
		}
	}

	// 파일 다운로드
	// https://localhost:9000/file/public/6e66dfc2ed10105104b56e3d2e2de1b7df.png?fn=vscode.png
	// https://localhost:9000/file/202104/12312541515151.xlsx?fn=다운받을원본파일명.xlsx
	@Get('/:path/:name')
	async download(
		@Res() res: Response,
		@Param('path') path: string,
		@Param('name') name: string,
		@Query('fn') fileName: string
	): Promise<void> {
		res.download(`${path}/${name}`, fileName)
	}
}

// "fileName": "vscode.png",
// "savedPath": "/public/6e66dfc2ed10105104b56e3d2e2de1b7df.png",
// "size": 130561
