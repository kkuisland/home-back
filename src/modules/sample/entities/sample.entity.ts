import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('samples')
export class SampleEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', comment: '인덱스' })
	id!: number

	@Column({ length: 20, comment: '샘플명' })
	title!: string

	@Column({ comment: '샘플 내용' })
	contents!: string

	@Column({ length: 20, comment: '스트링' })
	string!: string

	@Column({ type: 'datetime', width: 6, comment: '일자' })
	date!: Date

	@Column({ type: 'decimal', precision: 10, scale: 7, comment: '좌표' })
	letitude!: number
}
