import { Injectable } from '@nestjs/common'
import { createImageURL } from 'src/common/multer/options.multer'

@Injectable()
export class AppService {
	getHello(): string {
		return 'Hello World!'
	}

	uploadFiles(files: Express.Multer.File[]): string[] {
		const generatedFiles: string[] = []

		for (const file of files) {
			console.log(file)
			generatedFiles.push(createImageURL(file))
			// http://localhost:8080/public/파일이름 형식으로 저장이 됩니다.
		}
		console.log(generatedFiles)
		return generatedFiles
	}
}
