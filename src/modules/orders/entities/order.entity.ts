import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('order')
export class OrderEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: 'Aoto Index' })
	id!: number

	@Column({ length: 100, default: '', comment: '오더명' })
	name!: string

	@Column({ length: 100, default: '', comment: '설명' })
	description?: string
}
