import { ApiProperty } from '@nestjs/swagger'
import { IsString } from 'class-validator'
import { DtoDiscription } from '../../../common/discription/dto.discription'
export class CreateOrderDto {
	@IsString()
	@ApiProperty({
		description: DtoDiscription('이름', 'Order Title'),
		example: '구인수',
	})
	name!: string

	@IsString()
	@ApiProperty({
		description: DtoDiscription('설명', 'Order Contents'),
		example: '구인수 설명서',
		required: false,
	})
	description?: string
}
