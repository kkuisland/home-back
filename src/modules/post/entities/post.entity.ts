import {
	AfterLoad,
	BaseEntity,
	Column,
	CreateDateColumn,
	Entity,
	JoinColumn,
	OneToOne,
	PrimaryGeneratedColumn,
	UpdateDateColumn,
} from 'typeorm'
import { PostCategory } from '../enums/post-category.enum'
import { PhotoEntity } from '../../photo/entities/photo.entity'

@Entity('post')
export class PostEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: 'Auto Index' })
	id!: number

	@Column({ type: 'enum', enum: PostCategory, default: PostCategory.ETC, comment: '게시물 분류' })
	category!: string

	@Column({ length: 100, default: '', comment: '제목' })
	title!: string

	@Column({ length: 100, default: '', comment: '부 제목' })
	subtitle!: string

	@Column({ type: 'blob', default: '', comment: '내용' })
	contents!: string | Blob

	@OneToOne(() => PhotoEntity, { nullable: true, eager: true, persistence: false })
	@JoinColumn()
	photo!: PhotoEntity

	@CreateDateColumn({ comment: '생성 일시' })
	createdAt!: Date

	@UpdateDateColumn({ comment: '수정 일시' })
	updatedAt!: Date

	@AfterLoad()
	async contentsToString(): Promise<void> {
		this.contents = this.contents.toString()
	}
}
