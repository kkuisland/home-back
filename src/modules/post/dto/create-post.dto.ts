import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsIn, IsString, MaxLength } from 'class-validator'
import { DtoDiscription } from 'src/common/discription'
import { PostCategory } from '../enums/post-category.enum'
import { PhotoExtension } from '../../photo/enums/photo-extension.enum'
import { Transform } from 'class-transformer'

export class CreatePostDto {
	@IsEnum(PostCategory)
	@IsIn(Object.values(PostCategory))
	@ApiProperty({
		description: DtoDiscription('게시물 구분', Object.values(PostCategory) as []),
		example: PostCategory.ETC,
	})
	category!: string

	@IsString()
	@MaxLength(100)
	@Transform((params) => params.value.trim())
	@ApiProperty({
		description: DtoDiscription('게시물 제목', '100자 이내'),
		example: '제목',
	})
	title!: string

	@IsString()
	@MaxLength(100)
	@Transform((params) => params.value.trim())
	@ApiProperty({
		description: DtoDiscription('게시물 부 제목', '100자 이내'),
		example: '부 제목',
	})
	subtitle!: string

	@IsString()
	@ApiProperty({
		description: DtoDiscription('게시물 내용', '30자 이내'),
		example: '불편하면 고쳐주니? x 1000',
	})
	contents!: Blob | string

	@IsString()
	@MaxLength(30)
	@Transform((params) => params.value.trim())
	@ApiProperty({
		description: DtoDiscription('사진 이름', '30자 이내'),
		example: '사진 이름',
	})
	photoName!: string

	@IsEnum(PhotoExtension)
	@IsIn(Object.values(PhotoExtension))
	@Transform((params) => params.value.trim())
	@ApiProperty({
		description: DtoDiscription('사진 확장자', Object.values(PhotoExtension) as []),
		example: PhotoExtension.PNG,
	})
	photoExtension!: string

	@IsString()
	@Transform((params) => params.value.trim())
	@ApiProperty({
		description: DtoDiscription('사진 시스템 이름'),
		example: 'system12345',
	})
	photoFile!: string

	@IsString()
	@Transform((params) => params.value.trim())
	@ApiProperty({
		description: DtoDiscription('사진 파일 위치'),
		example: '/public/post/name',
	})
	photoUrl!: string
}
