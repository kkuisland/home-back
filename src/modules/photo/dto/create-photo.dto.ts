import { ApiProperty } from '@nestjs/swagger'
import { IsEnum, IsIn, IsString, MaxLength } from 'class-validator'
import { DtoDiscription } from 'src/common/discription'
import { PhotoExtension } from '../enums/photo-extension.enum'

export class CreatePhotoDto {
	@IsString()
	@MaxLength(30)
	@ApiProperty({
		description: DtoDiscription('사진 이름', '30자 이내'),
		example: '사진 이름',
	})
	photoName!: string

	@IsEnum(PhotoExtension)
	@IsIn(Object.values(PhotoExtension))
	@ApiProperty({
		description: DtoDiscription('사진 확장자', Object.values(PhotoExtension) as []),
		example: PhotoExtension.PNG,
	})
	photoExtension!: string

	@IsString()
	@ApiProperty({
		description: DtoDiscription('사진 시스템 이름'),
		example: 'system12345',
	})
	photoFile!: string

	@IsString()
	@ApiProperty({
		description: DtoDiscription('사진 파일 위치'),
		example: '/public/post/name',
	})
	photoUrl!: string
}
