import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm'
import { PhotoExtension } from '../enums/photo-extension.enum'

@Entity('photo')
export class PhotoEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: 'Auto Index' })
	id!: number

	@Column({ default: '', comment: '포토 이름' })
	photoName!: string

	@Column({ default: '', comment: '포토 시스템 이름' })
	photoFile!: string

	@Column({ default: '', comment: '파일 위치' })
	photoUrl!: string

	@Column({ type: 'enum', enum: PhotoExtension, default: PhotoExtension.PNG, comment: '사진 확장자' })
	extension!: string

	@CreateDateColumn({ comment: '생성 일시' })
	createdAt!: Date

	@UpdateDateColumn({ comment: '수정 일시' })
	updatedAt!: Date
}
