export enum PhotoExtension {
	PNG = 'png',
	JPEG = 'jpeg',
}
