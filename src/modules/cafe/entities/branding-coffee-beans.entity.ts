import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'

@Entity('branding_coffee_beans')
export class BrandingCoffeeBeansEntity extends BaseEntity {
	@PrimaryColumn({ length: 10, comment: '블랜딩 코드' })
	code!: string
	@Column({ type: 'int', comment: '용량' })
	volume!: number
	@Column({ type: 'int', comment: '구매 가격' })
	purchasePrice!: number
	@Column({ type: 'datetime', comment: '구매 일자' })
	purchaseDate!: Date
	@Column({ type: 'datetime', comment: '로스팅 일자' })
	roastingDate!: Date
	@Column({ type: 'datetime', comment: '유통기한' })
	shelfLife!: Date
}
