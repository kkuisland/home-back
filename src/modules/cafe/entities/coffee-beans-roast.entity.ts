import { BaseEntity, Column, Entity, PrimaryColumn } from 'typeorm'
import { RoastStep } from '../types/roast.type'

@Entity('coffee_beans_roast')
export class CoffeeBeansRoastEntity extends BaseEntity {
	@PrimaryColumn({ type: 'enum', enum: RoastStep, comment: '로스팅 단계' })
	step!: RoastStep
	@Column({ type: 'binary', comment: '로스팅 이미지' })
	image!: string
	@Column({ comment: '이미지 경로' })
	imagePath!: string
	@Column({ comment: '이미지 원본이름' })
	imageOriginal!: string
	@Column({ comment: '원두 맛' })
	taste!: string
	@Column({ comment: '원두 색상' })
	color!: string
	@Column({ comment: 'SCAA 분류법' })
	classification!: string
}
