import { BaseEntity, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { BeanStatus } from '../types/bean-status'
import { BeanGrade } from '../types/bean-grade'
import { RoastStep } from '../types/roast.type'
import { BeanProductionPlace } from '../types/bean-production-place'
import { BeanVarieties } from '../types/varieties'
import { BeanProcessingMethod } from '../types/bean-processing-method'
import { BrandingCoffeeBeansEntity } from './branding-coffee-beans.entity'
import { CoffeeBeansRoastEntity } from './coffee-beans-roast.entity'

@Entity('coffee_beans')
export class CoffeeBeansEntity extends BaseEntity {
	@PrimaryGeneratedColumn({ type: 'bigint', unsigned: true, comment: '커피 원두 인덱스' })
	id!: number
	@Column({ length: 50, comment: '원두 이름' })
	name!: string
	@Column({ type: 'enum', enum: BeanStatus, comment: '원두 상태' })
	status!: BeanStatus
	@Column({ type: 'enum', enum: BeanProductionPlace, comment: '원두 생산지' })
	productionPlace!: BeanProductionPlace
	@Column({ type: 'enum', enum: BeanVarieties, comment: '원두 품종' })
	varieties!: BeanVarieties
	@Column({ type: 'enum', enum: BeanProcessingMethod, comment: '원두 가공방식' })
	processingMethod!: BeanProcessingMethod
	@Column({ length: 10, comment: '수확기 xx ~ xx' })
	harvestSeason!: string
	@Column({ type: 'enum', enum: BeanGrade, comment: '원두 등급' })
	grade!: BeanGrade
	@Column({ length: 20, comment: '재배 고도 x,xxx ~ x,xxx m' })
	cultivationHeight!: string
	@Column({
		comment: '커핑 노트 다크초코렛: 쌉싸름한 쓴 맛, 땅콩: 땅콩 같은 고소함과 부드러움, 카라멜: 은은하게 느껴지는 단맛',
	})
	cuppingNotes!: string
	@Column({ type: 'tinyint', unsigned: true, comment: '신맛 1 ~ 5' })
	acidity!: number
	@Column({ type: 'tinyint', unsigned: true, comment: '쓴맛 1 ~ 5' })
	bitterness!: number
	@Column({ type: 'tinyint', unsigned: true, comment: '단맛 1 ~ 5' })
	sweetness!: number
	@Column({ type: 'tinyint', unsigned: true, comment: '바디 1 ~ 5' })
	body!: number
	@Column({ type: 'tinyint', unsigned: true, comment: '향미 1 ~ 5' })
	fragrance!: number
	@OneToOne(() => CoffeeBeansRoastEntity)
	@JoinColumn({ name: 'step' })
	roast!: RoastStep
	@Column({ default: '', comment: '타이틀' })
	title!: string
	@Column({ default: '', comment: '노트' })
	contents!: string
	@OneToOne(() => BrandingCoffeeBeansEntity)
	@JoinColumn({ name: 'code' })
	branding!: string
}
