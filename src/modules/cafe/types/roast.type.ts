export enum RoastStep {
	// 약 배전
	LIGHT = '라이트',
	CINNAMON = '시나몬',
	// 중 배전
	MEDIUM = '미디엄',
	HIGH = '하이', // regular동급
	REGULAR = '레귤러', // high동급
	CITY = '시티',
	FULLCITY = '풀-시티',
	// 강 배전
	FRENCH = '프렌치',
	ITALIAN = '이탈리안',
}
