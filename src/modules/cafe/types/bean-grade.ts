export enum BeanGrade {
	GRADE1 = '0 ~ 3',
	GRADE2 = '4 ~ 12',
	GRADE3 = '13 ~ 25',
	GRADE4 = '26 ~ 45',
	GRADE5 = '46 ~ 100',
	GRADE6 = '101 ~ 153',
	GRADE7 = '154 ~ 340',
	GRADE8 = '341 이상',
}
