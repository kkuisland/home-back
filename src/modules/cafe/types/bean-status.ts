export enum BeanStatus {
	BEANS = '원두',
	FRENCH_PRESS = '프렌치 프레스',
	DRIP_COFFEE_MAKER = '드립 & 커피 메이커',
	DUTCH = '더치',
	MOKA_POT = '모카포트',
	ESPRESSO = '에스프레소',
}
