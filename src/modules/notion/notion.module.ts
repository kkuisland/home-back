import { Module } from '@nestjs/common'
import { NotionController } from './notion.controller'

@Module({
	controllers: [NotionController],
	providers: [],
})
export class NotionModule {}
