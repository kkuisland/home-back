import { Controller, Get, Res } from '@nestjs/common'
import { Response } from 'express'
import { Public } from 'src/common/decorators/public.decorator'

@Controller('notion')
export class NotionController {
	@Get()
	@Public()
	root(@Res() res: Response) {
		return res.render('index', { name: 'index' })
	}
}
