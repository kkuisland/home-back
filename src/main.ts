import { NestFactory, Reflector } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from '@nestjs/common'
import { AuthGuard } from './common/guard/auth.guard'
import SetSwagger from './config/swagger.config'
import * as fs from 'fs'
import * as cookieParser from 'cookie-parser'
import * as compression from 'compression'
import { join } from 'path'
import { NestExpressApplication } from '@nestjs/platform-express'

declare const module: any
async function bootstrap() {
	// 개발 서버이면 https 적용 별도
	let httpsOptions
	try {
		httpsOptions = {
			key: fs.readFileSync('src/secrets/rootCA.key'),
			cert: fs.readFileSync('src/secrets/rootCA.pem'),
			passphrase: process.env.HTTPS_KEY,
		}
	} catch (error) {}

	const app = await NestFactory.create<NestExpressApplication>(
		AppModule,
		process.env.NODE_ENV === 'product'
			? { logger: ['error', 'verbose', 'warn'] }
			: { httpsOptions, logger: ['debug', 'error', 'verbose', 'warn'] }
	)
	app.useStaticAssets(join(__dirname, '..', 'public'))

	// Cors 옵션들
	const options = {
		origin: true,
		methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
		preflightContinue: false,
		credentials: true,
		allowedHeaders: 'Content-type, Accept',
	}
	app.enableCors(options)

	app.useGlobalPipes(
		new ValidationPipe({
			whitelist: true, // decorator(@)가 없는 속성이 들어오면 해당 속성은 제거하고 받아들입니다.
			forbidNonWhitelisted: true, // DTO에 정의되지 않은 값이 넘어오면 request 자체를 막는다.
			transform: true, // 클라이언트에서 값을 받자마자 타입을 정의한대로 자동 형변환한다.
			disableErrorMessages: true, // 자세한 오류 비활성화.
		})
	)

	app.use(cookieParser(process.env.COOKIE_SECRET))

	const reflector = app.get(Reflector)
	app.useGlobalGuards(new AuthGuard(reflector))

	app.setGlobalPrefix('api')

	// somewhere in your initialization file
	app.use(compression())

	SetSwagger(app)

	console.log(
		`[${process.env.NODE_ENV}] 환경에서 [${
			process.env.NODE_ENV === 'product'
				? `서버가 실행되었습니다...`
				: `${httpsOptions === undefined ? 'http' : 'https'}://localhost:${process.env.NODE_PORT}/api-docs`
		}] 서버를 시작합니다...`
	)

	await app.listen(process.env.NODE_ENV === 'product' ? 80 : parseInt(process.env.NODE_PORT))
	if (module.hot) {
		module.hot.accept()
		module.hot.dispose(() => app.close())
	}
}

bootstrap()
