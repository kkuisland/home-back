/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */

export class Friend {
	email!: string
	id!: string
	identity!: string
	mobile!: string
	name!: string
}

export abstract class IQuery {
	abstract getFriends(): Nullable<Nullable<Friend>[]> | Promise<Nullable<Nullable<Friend>[]>>
}

type Nullable<T> = T | null
