declare namespace NodeJS {
	interface ProcessEnv {
		NODE_ENV: string
		NODE_PORT: string
		NODE_URL: string
		TZ: string
		ATTACH_SAVE_PATH: string
		NODE_MAILER_ID: string
		NODE_MAILER_PW: string
		DB_KKUISLAND_HOST: string
		DB_KKUISLAND_PORT: string
		DB_KKUISLAND_USERNAME: string
		DB_KKUISLAND_PASSWORD: string
		DB_KKUISLAND_DATABASE: string
		MONGODB_URL: string
		COOKIE_NAME: string
		JWT_SECRET: string
		JWT_EXPIRATION_TIME: string
		COOKIE_SECRET: string
		COOKIE_EXPIRATION_TIME: string
		HTTPS_KEY: string
	}
}
