export const DtoDiscription = (title: string, lineOne?: string | [], lineTwo?: string): string => {
	let lineOneHead = ''
	if (lineOne !== undefined) {
		if (typeof lineOne === 'string') {
			const tempOne = lineOne.trim()
			switch (tempOne[0]) {
				case '/':
					lineOneHead = 'regex: '
					break
				case '[':
					lineOneHead = 'enum: '
					break

				default:
					lineOneHead = 'option: '
					break
			}
		} else {
			lineOneHead = 'enum: '
		}
	}

	return `${title}\n
	${lineOneHead}${lineOne ?? ''}
	${lineTwo ?? ''}`
}

export const ApiDiscription = (title: string, lineOne?: string, lineTwo?: string, lineThree?: string): string => {
	return `${title}\n
	${lineOne ?? ''}
	${lineTwo ?? ''}
	${lineThree ?? ''}`
}
