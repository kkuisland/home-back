import { InternalServerErrorException } from '@nestjs/common'
import { QueryFailedError } from 'typeorm'
import { CommonNotice } from '../enums'

export const QueryException = {
	create: (error: unknown): never => {
		throw new InternalServerErrorException(
			error instanceof QueryFailedError ? error.message : CommonNotice.UPDATE_FAILED
		)
	},
}
