import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException } from '@nestjs/common'
import { validate } from 'class-validator'
import { ValidateNested } from 'class-validator'

export class WrapperArrayDto {
	@ValidateNested({ each: true })
	list!: any

	constructor(list: any) {
		this.list = list
	}
}

@Injectable()
export class ArrayDtoPipe implements PipeTransform<any> {
	constructor(private dto: any) {
		this.dto = dto
	}

	async transform(value: any, { metatype }: ArgumentMetadata): Promise<any> {
		if (!metatype || this.toValidate(metatype)) {
			return value
		}
		if (!(value instanceof Array)) {
			return value
		}
		const subClasses = Array(value.length)
			.fill(null)
			.map((x, index) => new this.dto(value[index]))

		const wrapperClass = new WrapperArrayDto(subClasses)
		const validationErrors = await validate(wrapperClass)
		if (validationErrors.length === 0) {
		} else {
			throw new BadRequestException(
				validationErrors.map((one) =>
					one.children?.map((two) => {
						console.error(two.children)
						return two.children
					})
				)
			)
		}

		return value
	}

	private toValidate(metatype: Function): boolean {
		const types: Function[] = [String, Boolean, Number, Array, Object]
		return !types.includes(metatype)
	}
}
