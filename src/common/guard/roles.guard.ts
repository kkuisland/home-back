import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { Observable } from 'rxjs'
import { FriendEntity } from 'src/modules/auth/entities/friend.entity'

@Injectable()
export class RolesGuard implements CanActivate {
	constructor(private readonly reflector: Reflector) {}

	canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
		const roles = this.reflector.get<string[]>('roles', context.getHandler())
		if (!roles) {
			return true
		}
		const request = context.switchToHttp().getRequest()
		const friends = request.friend as FriendEntity

		// return friends && friends.roleIds && friends.roleIds.some((role) => roles.includes(role))
		return true
	}
}
