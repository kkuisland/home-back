import { ExecutionContext, Injectable } from '@nestjs/common'
import { Reflector } from '@nestjs/core'
import { AuthGuard as NestAuthGuard } from '@nestjs/passport'
import { Observable } from 'rxjs'
@Injectable()
export class AuthGuard extends NestAuthGuard('jwt') {
	constructor(private readonly reflector: Reflector) {
		super()
	}
	override canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean> {
		const isPublic = this.reflector.get<boolean>('isPublic', context.getHandler())

		if (isPublic) {
			return true
		}
		return super.canActivate(context)
	}
}
