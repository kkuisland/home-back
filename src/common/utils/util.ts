export const KKuUtil = {
	removeHyphenPhoneNumbers: (phoneNumber: string): string => {
		// const phoneNubmerWithoutHyphen = phoneNumber.replace(/-/gi, '');
		const phoneNubmerWithoutHyphen = phoneNumber.replace(/[^0-9]/g, '')

		return phoneNubmerWithoutHyphen
	},

	addHyphenPhoneNumber: (phoneNumber: string): string => {
		const phone = phoneNumber.replace(/[^0-9]/g, '')
		const phoneNumberWithHyphen = phone.replace(/^(\d{2,3})(\d{3,4})(\d{4})$/, `$1-$2-$3`)
		return phoneNumberWithHyphen
	},
}
