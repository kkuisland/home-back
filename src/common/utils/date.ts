const KKuDate = {
	year: (): string => {
		const gDate = new Date()
		gDate.setHours(gDate.getHours() + 9)
		return gDate.getFullYear().toString()
	},

	IosToKr: (gDate?: string | Date): Date => {
		const date = KKuDate.getKrDate(gDate)
		return date
	},

	/**
	 * 날짜 포멧 변환 IOS to YYYY-MM-DD HH:mm:ss
	 * @param gDate string
	 * @returns YYYY-MM-DD HH:mm:ss
	 */
	IosToYYYYMMDDHHmmss: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		return date.toISOString().substring(0, 19).replace('T', ' ')
	},

	/**
	 * 날짜 포멧 변환 IOS to YYYY-MM-DD
	 * @param gDate string
	 * @returns YYYY-MM-DD
	 */
	IosToYYYYMMDD: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		return date.toISOString().substring(0, 10)
	},

	/**
	 * 날짜 포멧 IOS YYYY-MM-DD HH:mm:ss.zzz
	 * @param gDate string
	 * @returns YYYY-MM-DD HH:mm:ss.zzz
	 */
	YYYYMMDDHHmmssZ: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		return date.toISOString()
	},

	/**
	 * 날짜 포멧 YYYY-MM-DD HH:mm:ss 제공
	 * @param gDate Date
	 * @returns YYYY-MM-DD HH:mm:ss
	 */
	YYYYMMDDHHmmss: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		return (
			date.getFullYear() +
			'-' +
			('0' + (date.getMonth() + 1)).slice(-2) +
			'-' +
			date.getDate() +
			' ' +
			('0' + date.getHours()).slice(-2) +
			':' +
			('0' + date.getMinutes()).slice(-2) +
			':' +
			('0' + date.getSeconds()).slice(-2)
		)
	},

	/**
	 * 날짜 포멧 YYYY-MM-DD HH 제공
	 * @param gDate Date
	 * @returns YYYY-MM-DD HH
	 */
	YYYYMMDDHH: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		// console.debug(date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getDate());
		return (
			date.getFullYear() +
			'-' +
			('0' + (date.getMonth() + 1)).slice(-2) +
			'-' +
			('0' + date.getDate()).slice(-2) +
			' ' +
			('0' + date.getHours()).slice(-2)
		)
	},

	/**
	 * 날짜 포멧 YYYY-MM-DD 제공
	 * @param gDate Date
	 * @returns YYYY-MM-DD
	 */
	YYYYMMDD: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		// console.debug(date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getDate());
		return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getDate()
	},

	/**
	 * 양계인 IOT 파티션 조회용
	 * @returns 현재날짜기준 + 1 =>  포멧은 20220621
	 */
	YYYYMMDD_ADDONEDAY: (gDate?: string | Date): string => {
		const date = KKuDate.getKrDate(gDate)
		return date.getFullYear() + ('0' + (date.getMonth() + 1)).slice(-2) + date.getDate()
	},

	/**
	 * 전달 받은 시간 대비 남은 시간 알림
	 * @param date 저장시간 + 5분
	 * @returns 남은 시간 문자열
	 */
	remainingTime: (date: string | Date): string => {
		// 5분 더하기
		const nowDate = new Date()
		const oldDate = new Date(date)
		const remaining = oldDate.getTime() - nowDate.getTime()
		const diffDay = Math.floor(remaining / (1000 * 60 * 6 * 24))
		const diffhour = Math.floor((remaining % (1000 * 60 * 6 * 24)) / (1000 * 60 * 60))
		const diffMin = Math.floor((remaining % (1000 * 60 * 60)) / (1000 * 60))
		const diffSec = Math.floor((remaining % (1000 * 60)) / 1000)
		let totalString = ''
		if (diffDay !== 0) {
			totalString += diffDay + '일 '
		} else if (diffhour !== 0) {
			totalString += diffhour + '시 '
		}
		return `${totalString}${diffMin}분 ${diffSec}초 남았습니다.`
	},

	// 5분 추가
	addMin5Time: (): Date => {
		const date = new Date()
		date.setMinutes(date.getMinutes() + 5)
		return date
	},

	getKrDate: (gDate?: string | Date): Date => {
		let date = null
		if (!gDate) {
			date = new Date()
			date.setHours(date.getHours() + 9)
		} else if (typeof gDate === 'string') {
			// 문자열로 들어오면 한국시간으로 변환 작업이 필요하다.   -9 시간으로 저장
			date = new Date(gDate)
			date.setHours(date.getHours() + 9)
		} else {
			// 날짜 포멧으로 들어오면 그시가 그대로 저장 된다.
			date = new Date(gDate)
		}

		return date
	},
}

export default KKuDate
