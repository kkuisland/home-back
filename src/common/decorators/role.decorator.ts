import { CustomDecorator, SetMetadata } from '@nestjs/common'
import { FriendRole } from 'src/common/enums/friend-role.enum'

export const Roles = (...role: FriendRole[]): CustomDecorator<string> => SetMetadata('roles', role)
