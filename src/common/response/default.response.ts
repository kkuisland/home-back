import { HttpStatus } from '@nestjs/common'

const ResponseSend = {
	default: (statusCode: HttpStatus, message: string, data?: any) => {
		console.log('\n👀 ===== Response Start ===== 👀\n')
		console.log({ statusCode, message, ...data })
		console.log('\n👀 ===== Response End   ===== 👀\n')
		return { statusCode: statusCode, message: message, ...data }
	},
}

export default ResponseSend
