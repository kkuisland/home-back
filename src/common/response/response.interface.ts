import { HttpStatus } from '@nestjs/common'

export interface ResponseSendDefault {
	statusCode: HttpStatus
	message: string
	data: any
}
