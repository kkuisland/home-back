export enum CommonNotice {
	CREATED_SEND = '생성 되었습니다.',
	FINDONE_SEND = '검색을 완료했습니다.',
	FINDALL_SEND = '검색목록을 찾았습니다.',
	UPDATED_SEND = '업데이트 되었습니다.',
	DELETE_SEND = '삭제 되었습니다.',
	BAD_REQUEST = '아무런 일도 일어나지 않았습니다.',
	CREATION_FAILED = '생성에 실패했습니다.',
	NOT_FOUND = '해당 결과를 찾을 수 없습니다.',
	UPDATE_FAILED = '업데이트에 실패했습니다.',
	UNAUTHORIZED = '일치하는 정보가 없습니다.',
}
