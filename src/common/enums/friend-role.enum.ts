export enum FriendRole {
	ADMIN = '관리자',
	MASTER = '마스터',
	FRIEND = '친구',
}
