export enum FriendNotice {
	IDENTITY = '소문자 시작 + 소문자, 숫자 조합으로 2~20자로 작성해주세요.',
	NAME = '영문, 한글 2~20자로 작성해주세요.',
	EMAIL = '이메일은 [ xxx@gmail.com ] 형식 3~20자로 작성해주세요.',
	MOBILE = '핸드폰은 [ 01x-xxxx-xxxx ] 형식으로 작성해주세요.',
	PASSWORD = '최소 6자, 문자시작 + 문자, 숫자, 특수문자[.$`~!@$!%*#^?&\\(\\)-_=+] 조합으로 6~20자로 작성해주세요.',
	NOT_FOUND = '검색된 정보가 없습니다.',
	CONFLICT_ID = '해당 아이디가 이미 존재합니다.',
	CONFLICT_EMAIL = '해당 이메일이 이미 존재합니다.',
	CONFLICT_MOBILE = '해당 번호가 이미 존재합니다.',
}
