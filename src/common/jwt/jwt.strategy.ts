import { Injectable, UnauthorizedException } from '@nestjs/common'
import { PassportStrategy } from '@nestjs/passport'
import { Request } from 'express'
import { Strategy, VerifiedCallback } from 'passport-jwt'
import { AuthService } from 'src/modules/auth/auth.service'
import { Payload } from 'src/modules/auth/types/payload.type'
import { CommonNotice } from 'src/common/enums'

const fromAuthCookie = () => {
	return function (req: Request) {
		let token = null
		if (req && req.cookies) {
			token = req.signedCookies[process.env.COOKIE_NAME]
		}
		return token
	}
}

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	constructor(private readonly authService: AuthService) {
		super({ jwtFromRequest: fromAuthCookie(), ignoreExpiration: true, secretOrKey: process.env.JWT_SECRET })
	}

	async validate(payload: Payload, done: VerifiedCallback): Promise<void> {
		const friend = await this.authService.tokenValidateFriend(payload)
		if (!friend) {
			return done(new UnauthorizedException(CommonNotice.UNAUTHORIZED))
		}
		return done(null, friend)
	}
}
