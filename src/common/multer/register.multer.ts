import { MulterModuleOptions } from '@nestjs/platform-express'
import { diskStorage } from 'multer'
// 오늘날짜 포맷팅 쉽게 할려고 가볍게 쓸 수 있는것 하나 설치해봄
import * as fs from 'fs'
import { extname } from 'path'

const multerRegister: MulterModuleOptions = {
	dest: '/public',
	storage: diskStorage({
		// 파일저장위치 + 년월 에다 업로드 파일을 저장한다.
		// 요 부분을 원하는 데로 바꾸면 된다.
		destination(req, file, callback) {
			const dest = `${process.env.ATTACH_SAVE_PATH}`

			if (!fs.existsSync(dest)) {
				fs.mkdirSync(dest, { recursive: true })
			}

			callback(null, dest)
		},
		filename: (req, file, cb) => {
			// 업로드 후 저장되는 파일명을 랜덤하게 업로드 한다.(동일한 파일명을 업로드 됐을경우 오류방지)
			const randomName = Array(32)
				.fill(null)
				.map(() => Math.round(Math.random() * 16).toString(16))
				.join('')
			return cb(null, `${randomName}${extname(file.originalname)}`)
		},
	}),
}

export default multerRegister
