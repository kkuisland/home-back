import { NotFoundException } from '@nestjs/common'
import { existsSync, mkdirSync } from 'fs'
import { diskStorage } from 'multer'
import { extname } from 'path'
import { v4 as uuid } from 'uuid'

const uuidRandom = (file: any): string => {
	const uuidPath = `${uuid()}${extname(file.originalname)}`
	return uuidPath
}
export const multerOptions = {
	fileFilter: (request: any, file: any, callback: any) => {
		if (file.mimetype.match(/\/(jpg|jpeg|png)$/)) {
			// 이미지 형식은 jpg, jpeg, png만 허용합니다.
			callback(null, true)
		} else {
			callback(new NotFoundException('지원하지 않는 이미지 형식입니다.'), false)
		}
	},

	storage: diskStorage({
		destination: (request: any, file: any, callback: any) => {
			const uploadPath = 'public'

			if (!existsSync(uploadPath)) {
				// public 폴더가 존재하지 않을시, 생성합니다.
				mkdirSync(uploadPath)
			}

			callback(null, uploadPath)
		},

		filename: (request: any, file: any, callback: any) => {
			callback(null, uuidRandom(file))
		},
	}),
}

export const createImageURL = (file: any): string => {
	const serverAddress = 'https://localhost:9000'

	// 파일이 저장되는 경로: 서버주소/public 폴더
	// 위의 조건에 따라 파일의 경로를 생성해줍니다.
	return `${serverAddress}/public/${file.fieldname}`
}
