import { GraphQLDefinitionsFactory } from '@nestjs/graphql'
import { join } from 'path'

const definitionsFactory = new GraphQLDefinitionsFactory()
definitionsFactory.generate({
	typePaths: [join(__dirname.replace('config', '') + '/modules/**/graphql/*.graphql')],
	path: join(process.cwd(), 'src/graphql.ts'),
	outputAs: 'class',
})
