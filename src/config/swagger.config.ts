import { INestApplication } from '@nestjs/common'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import { CafeModule } from '../modules/cafe/cafe.module'
import { AuthModule } from '../modules/auth/auth.module'

const swaggerMenus = [
	{ name: 'Home', url: '/api-docs/all', version: '1.0', module: [] },
	{ name: 'Auth', url: '/api-docs/auth', version: '1.0', module: [AuthModule] },
	{ name: 'Cafe', url: '/api-docs/cafe', version: '1.0', module: [CafeModule] },
]

const SetSwagger = (app: INestApplication): void => {
	for (const menu of swaggerMenus) {
		const config = new DocumentBuilder()
			.setTitle(`KKu' Island ${menu.name}API Doc`)
			.setDescription(
				`The KKu' Island ${menu.name}API Description
 				<hr />
				${swaggerMenus.flatMap((item) => {
					return `　🧙‍♂️${item.name} : <a href="${process.env.NODE_URL}:${process.env.NODE_PORT}${item.url}">${item.name}</a>`
				})}`
			)
			.setVersion(`${menu.version}`)
			.build()
		const document = SwaggerModule.createDocument(app, config, { include: menu.module })

		SwaggerModule.setup(`${menu.url}`, app, document, {
			swaggerOptions: { defaultModelsExpandDepth: 0 },
		})
	}
}

export default SetSwagger
