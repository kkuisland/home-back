import { DataSourceOptions } from 'typeorm'
import { join } from 'path'
import { FriendEntity } from '../modules/auth/entities/friend.entity'
import { LoggerEntity } from '../modules/auth/entities/logger.entity'
import { PasswordEntity } from '../modules/auth/entities/passwords.entity'
import { RoleEntity } from '../modules/auth/entities/role.entity'
import { BrandingCoffeeBeansEntity } from '../modules/cafe/entities/branding-coffee-beans.entity'
import { CoffeeBeansEntity } from '../modules/cafe/entities/coffee-beans.entity'
import { CoffeeBeansRoastEntity } from '../modules/cafe/entities/coffee-beans-roast.entity'
import { SampleEntity } from '../modules/sample/entities/sample.entity'
import { SnakeNamingStrategy } from 'typeorm-naming-strategies'
import { EnvData } from './env.config'
import { TestEntity } from '../modules/auth/entities/test.entity'
import { PhotoEntity } from 'src/modules/photo/entities/photo.entity'
import { PostEntity } from 'src/modules/post/entities/post.entity'
import { OrderEntity } from 'src/modules/orders/entities/order.entity'

// 구조를 나눠서 사용
const data = EnvData()
// TypeORM 기본값 세팅
let entitiesConfig
if (data.NODE_ENV === 'development') {
	entitiesConfig = [
		FriendEntity,
		LoggerEntity,
		PasswordEntity,
		RoleEntity,
		BrandingCoffeeBeansEntity,
		CoffeeBeansEntity,
		CoffeeBeansRoastEntity,
		SampleEntity,
		TestEntity,
		OrderEntity,
		PostEntity,
		PhotoEntity,
	]
} else {
	entitiesConfig = [join(__dirname.replace('config', ''), '/modules/**/entities/*{.ts,.js}')]
}

export const KKuIslandDataSource: DataSourceOptions = {
	name: 'default',
	type: 'mariadb',
	host: data.DB_KKUISLAND_HOST,
	port: parseInt(data.DB_KKUISLAND_PORT),
	username: data.DB_KKUISLAND_USERNAME,
	password: data.DB_KKUISLAND_PASSWORD,
	database: data.DB_KKUISLAND_DATABASE,
	// entities: [join(__dirname, '/modules/**/entities/*{.ts,.js}')],
	entities: entitiesConfig,
	// Run migrations automatically,
	// you can disable this if you prefer running migration manually.
	// migrationsRun: false,
	logging: false,
	logger: 'file',
	// Allow both start:prod and start:dev to use migrations
	// __dirname is either dist or src folder, meaning either
	// the compiled js in prod or the ts in dev.
	// migrations: ['dist/migrations/*{.ts,.js}'],
	// cli: {
	// 	// Location of migration should be inside src folder
	// 	// to be compiled into dist/ folder.
	// 	migrationsDir: 'src/migrations',
	// },
	// migrationsTableName: 'migrations',
	// Snake Strategy 사용
	namingStrategy: new SnakeNamingStrategy(),
	// We are using migrations, synchronize should be set to false.
	synchronize: true,
}
