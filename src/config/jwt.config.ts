import { JwtModuleOptions } from '@nestjs/jwt'
import { EnvData } from './env.config'

//Config 구분
const data = EnvData()
export const KKuIslandJwtOptions: JwtModuleOptions = {
	secret: data.JWT_SECRET,
	signOptions: { expiresIn: `${data.JWT_EXPIRATION_TIME ?? 3}d` },
}
