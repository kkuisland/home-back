import * as dotenv from 'dotenv'
import * as fs from 'fs'

export const EnvData = (): NodeJS.ProcessEnv => {
	try {
		return dotenv.parse(fs.readFileSync(`.env`)) as NodeJS.ProcessEnv
	} catch (error) {
		return dotenv.parse(fs.readFileSync(`.product.env`)) as NodeJS.ProcessEnv
	}
}
