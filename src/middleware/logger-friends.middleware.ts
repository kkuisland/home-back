import { Injectable, NestMiddleware } from '@nestjs/common'

@Injectable()
export class LoggerFriendMiddleware implements NestMiddleware {
	use(req: any, res: any, next: () => void) {
		console.log('Friend 전용 로거 입니다.....')
		next()
	}
}
