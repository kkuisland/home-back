import { Injectable, NestMiddleware } from '@nestjs/common'
import { JwtService } from '@nestjs/jwt'
import { InjectRepository } from '@nestjs/typeorm'
import { Request, Response } from 'express'
import KKuDate from 'src/common/utils/date'
import { LoggerEntity } from 'src/modules/auth/entities/logger.entity'
import { Payload } from 'src/modules/auth/types/payload.type'
import { Repository } from 'typeorm'

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
	constructor(
		private readonly jwtService: JwtService,
		@InjectRepository(LoggerEntity) private readonly friendsLoggerRepository: Repository<LoggerEntity>
	) {}
	async use(req: Request, res: Response, next: () => void): Promise<void> {
		const logger = {
			url: req.originalUrl,
			friends: '',
			name: '',
			timestap: KKuDate.YYYYMMDDHHmmss(),
		}
		if (!req.signedCookies[process.env.COOKIE_NAME]) {
			console.log('비회원 👨 => ', logger)
		} else {
			const jwt = req.signedCookies[process.env.COOKIE_NAME]
			const friends: Payload = this.jwtService.decode(jwt) as Payload

			await this.friendsLoggerRepository.save({
				identity: friends.identity,
				baseUrl: req.baseUrl,
				originalUrl: req.originalUrl,
			})
			logger.friends = friends.identity
			logger.name = friends.name
			// console.log('call => ', logger)
		}
		next()
	}
}
